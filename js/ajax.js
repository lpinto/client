// Premier test AJAX/JQuery
let callGetSuccessDep = function (response) {
    for (let i = 0; i < response.length; i++) {
        ajoutLigne(response[i]);
    }

};

function ajoutLigne(response) {
    let tableau = document.getElementById("tableau");

    let ligne = tableau.insertRow(-1);//on a ajouté une ligne

    let colonne1 = ligne.insertCell(0);//on a une ajouté une cellule
    let textCol1 = document.createTextNode(response.commune);
    console.log(textCol1);
    colonne1.appendChild(textCol1);

    let colonne2 = ligne.insertCell(1);//on ajoute la seconde cellule
    let textCol2 = document.createTextNode(response.elemPatri);
    colonne2.appendChild(textCol2);
}

function buttonClickGET() {

    $(function () {

        let departement = document.getElementById("departement").value;
        let commune = document.getElementById("commune").value;
        let critere = document.getElementById("critere").value;
        console.log(critere);

        if (departement !== "") {
            let url = "https://127.0.0.1:8000/api/departement/" + departement;
            // En cas de succès c'est à dire un retour
            $.get(url, callGetSuccessDep).done(function () {

            })
                // En cas d'echec...
                .fail(function (error) {
                    alert("Echec de la requête : " + JSON.stringify(error));
                })
        } else if (commune !== "") {
            let url = "https://127.0.0.1:8000/api/commune/" + commune;
            // En cas de succès c'est à dire un retour
            $.get(url, callGetSuccessDep).done(function () {

            })
                // En cas d'echec...
                .fail(function (error) {
                    alert("Echec de la requête : " + JSON.stringify(error));
                })
        } else if (critere !== "") {
            let url = "https://127.0.0.1:8000/api/critere/" + critere;
            // En cas de succès c'est à dire un retour
            $.get(url, callGetSuccessDep).done(function () {

            })
                // En cas d'echec...
                .fail(function (error) {
                    alert("Echec de la requête : " + JSON.stringify(error));
                })
        }
    });
}
